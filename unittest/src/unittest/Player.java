package unittest;

public class Player {
	private char name;
	private int win;

	private int lose;
	private int draw;

	public Player(char name) {
		super();
		this.name = name;
	}

	public char getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Player [name=" + name + "]";
	}

	public int getWin() {
		return win;
	}

	public int getLose() {
		return lose;
	}

	public int getDraw() {
		return draw;
	}
	public void win() {
		this.win++;
	}
	public void lose() {
		this.lose++;
	}
	public void draw() {
		this.draw++;
	}

}